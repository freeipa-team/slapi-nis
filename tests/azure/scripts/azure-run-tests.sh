#!/bin/bash -eux

if [ $# -ne 1 ]; then
    echo "Docker environment ID is not provided"
    exit 1
fi

PROJECT_ID="$1"
BUILD_REPOSITORY_LOCALPATH="${BUILD_REPOSITORY_LOCALPATH:-$(realpath .)}"

PROJECT_TESTS_TO_RUN_VARNAME="PROJECT_TESTS_TO_RUN_${PROJECT_ID}"
PROJECT_TESTS_TO_RUN="${!PROJECT_TESTS_TO_RUN_VARNAME:-}"
# in case of missing explicit list of tests to be run the Pytest run all the
# discovered tests, this is an error for this CI
[ -z "$PROJECT_TESTS_TO_RUN" ] && { echo 'Nothing to test'; exit 1; }

PROJECT_TESTS_ENV_NAME_VARNAME="PROJECT_TESTS_ENV_NAME_${PROJECT_ID}"
PROJECT_TESTS_ENV_NAME="${!PROJECT_TESTS_ENV_NAME_VARNAME:-}"
[ -z "$PROJECT_TESTS_ENV_NAME" ] && \
    { echo "Project name is not set for project:${PROJECT_ID}"; exit 1 ;}

PROJECT_TESTS_TYPE_VARNAME="PROJECT_TESTS_TYPE_${PROJECT_ID}"
PROJECT_TESTS_TYPE="${!PROJECT_TESTS_TYPE_VARNAME:-integration}"

# Normalize spacing and expand the list afterwards. Remove {} for the single list element case
PROJECT_TESTS_TO_RUN=$(eval "echo {$(echo $PROJECT_TESTS_TO_RUN | sed -e 's/[ \t]+*/,/g')}" | tr -d '{}')

PROJECT_TESTS_TO_IGNORE_VARNAME="PROJECT_TESTS_TO_IGNORE_${PROJECT_ID}"
PROJECT_TESTS_TO_IGNORE="${!PROJECT_TESTS_TO_IGNORE_VARNAME:-}"
[ -n "$PROJECT_TESTS_TO_IGNORE" ] && \
PROJECT_TESTS_TO_IGNORE=$(eval "echo --ignore\ {$(echo $PROJECT_TESTS_TO_IGNORE | sed -e 's/[ \t]+*/,/g')}" | tr -d '{}')

PROJECT_TESTS_CLIENTS_VARNAME="PROJECT_TESTS_CLIENTS_${PROJECT_ID}"
PROJECT_TESTS_CLIENTS="${!PROJECT_TESTS_CLIENTS_VARNAME:-0}"

PROJECT_TESTS_REPLICAS_VARNAME="PROJECT_TESTS_REPLICAS_${PROJECT_ID}"
PROJECT_TESTS_REPLICAS="${!PROJECT_TESTS_REPLICAS_VARNAME:-0}"

PROJECT_TESTS_CONTROLLER="${PROJECT_ID}_master_1"
PROJECT_TESTS_LOGSDIR="${PROJECT_TESTS_REPO_PATH}/ipa_envs/${PROJECT_TESTS_ENV_NAME}/${CI_RUNNER_LOGS_DIR}"

PROJECT_TESTS_DOMAIN="${PROJECT_TESTS_DOMAIN:-ipa.test}"
# bash4
PROJECT_TESTS_REALM="${PROJECT_TESTS_DOMAIN^^}"

# for base tests only 1 master is needed even if another was specified
if [ "$PROJECT_TESTS_TYPE" == "base" ]; then
    PROJECT_TESTS_CLIENTS="0"
    PROJECT_TESTS_REPLICAS="0"
fi

project_dir="${PROJECT_TESTS_ENV_WORKING_DIR}/${PROJECT_TESTS_ENV_NAME}"
ln -sfr \
    "${PROJECT_TESTS_DOCKERFILES}/docker-compose.yml" \
    "$project_dir"/

ln -sfr \
    "${PROJECT_TESTS_DOCKERFILES}/seccomp.json" \
    "$project_dir"/

# will be generated later in setup_containers.py
touch "${project_dir}"/ipa-test-config.yaml

pushd "$project_dir"

BUILD_REPOSITORY_LOCALPATH="$BUILD_REPOSITORY_LOCALPATH" \
IPA_DOCKER_IMAGE="${IPA_DOCKER_IMAGE:-project-azure-builder}" \
IPA_NETWORK="${IPA_NETWORK:-ipanet}" \
IPA_IPV6_SUBNET="2001:db8:1:${PROJECT_ID}::/64" \
docker-compose -p "$PROJECT_ID" up \
    --scale replica="$PROJECT_TESTS_REPLICAS" \
    --scale client="$PROJECT_TESTS_CLIENTS" \
    --force-recreate --remove-orphans -d

popd

PROJECT_TESTS_CLIENTS="$PROJECT_TESTS_CLIENTS" \
PROJECT_TESTS_REPLICAS="$PROJECT_TESTS_REPLICAS" \
PROJECT_TESTS_ENV_ID="$PROJECT_ID" \
PROJECT_TESTS_ENV_WORKING_DIR="$PROJECT_TESTS_ENV_WORKING_DIR" \
PROJECT_TESTS_ENV_NAME="$PROJECT_TESTS_ENV_NAME" \
IPA_TEST_CONFIG_TEMPLATE="${BUILD_REPOSITORY_LOCALPATH}/tests/azure/templates/ipa-test-config-template.yaml" \
PROJECT_TESTS_REPO_PATH="$PROJECT_TESTS_REPO_PATH" \
PROJECT_TESTS_DOMAIN="$PROJECT_TESTS_DOMAIN" \
python3 setup_containers.py

# path to runner within container
tests_runner="${PROJECT_TESTS_REPO_PATH}/${PROJECT_TESTS_SCRIPTS}/azure-run-${PROJECT_TESTS_TYPE}-tests.sh"

tests_result=1
{ docker exec -t \
    --env PROJECT_TESTS_SCRIPTS="${PROJECT_TESTS_REPO_PATH}/${PROJECT_TESTS_SCRIPTS}" \
    --env IPA_PLATFORM="$IPA_PLATFORM" \
    --env PROJECT_TESTS_DOMAIN="$PROJECT_TESTS_DOMAIN" \
    --env PROJECT_TESTS_REALM="$PROJECT_TESTS_REALM" \
    --env PROJECT_TESTS_LOGSDIR="$PROJECT_TESTS_LOGSDIR" \
    --env PROJECT_TESTS_TO_RUN="$PROJECT_TESTS_TO_RUN" \
    --env PROJECT_TESTS_TO_IGNORE="$PROJECT_TESTS_TO_IGNORE" \
    "$PROJECT_TESTS_CONTROLLER" \
    /bin/bash --noprofile --norc \
    -eux "$tests_runner" && tests_result=0 ; } || tests_result=$?

pushd "$project_dir"
BUILD_REPOSITORY_LOCALPATH="$BUILD_REPOSITORY_LOCALPATH" \
IPA_DOCKER_IMAGE="${IPA_DOCKER_IMAGE:-project-azure-builder}" \
IPA_NETWORK="${IPA_NETWORK:-ipanet}" \
IPA_IPV6_SUBNET="2001:db8:1:${PROJECT_ID}::/64" \
docker-compose -p "$PROJECT_ID" down
popd

exit $tests_result
