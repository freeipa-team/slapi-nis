#!/bin/bash -eux

# this script is intended to be run within container
#
# distro-specifics
source "${PROJECT_TESTS_SCRIPTS}/variables.sh"

rm -rf "$PROJECT_TESTS_LOGSDIR"
mkdir "$PROJECT_TESTS_LOGSDIR"
pushd "$PROJECT_TESTS_LOGSDIR"

tests_result=1
{ IPATEST_YAML_CONFIG=~/.ipa/ipa-test-config.yaml \
    ipa-run-tests \
    --logging-level=debug \
    --logfile-dir="$PROJECT_TESTS_LOGSDIR" \
    --with-xunit \
    --verbose \
    $PROJECT_TESTS_TO_IGNORE \
    $PROJECT_TESTS_TO_RUN && tests_result=0 ; } || \
    tests_result=$?

# fix permissions on logs to be readable by Azure's user (vsts)
chmod -R o+rX "$PROJECT_TESTS_LOGSDIR"

find "$PROJECT_TESTS_LOGSDIR" -mindepth 1 -maxdepth 1 -not -name '.*' -type d \
    -exec tar --remove-files -czf {}.tar.gz {} \;

echo "Report memory statistics"
cat /sys/fs/cgroup/memory/memory.memsw.failcnt ||:
cat /sys/fs/cgroup/memory/memory.memsw.limit_in_bytes ||:
cat /sys/fs/cgroup/memory/memory.memsw.max_usage_in_bytes ||:
cat /sys/fs/cgroup/memory/memory.failcnt ||:
cat /sys/fs/cgroup/memory/memory.max_usage_in_bytes ||:
cat /sys/fs/cgroup/memory/memory.limit_in_bytes ||:
cat /proc/sys/vm/swappiness ||:

exit $tests_result
