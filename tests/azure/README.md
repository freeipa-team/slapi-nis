Test with azure pipelines

Add pipeline definition that can be used to run build and test jobs in
dev.azure.com environment.

Following needs to be done to use the Azure Pipelines definition for
slapi-nis:

- Configure Azure DevOps organization
- Create public project there
- Clone your Git repository for slapi-nis from pagure.io
- On Azure side, navigate to Repos -> Files, choose your repo top
  directory and click 'Clone'
- The 'Clone Repository' dialog will show how to clone the repository
  with HTTPS or SSH. Choose SSH tab and note the URL.
- Set mirroring hook on Pagure's project site (Settings -> Hooks ->
  Mirroring) by providing this URL
- Return back to the hook settings in Pagure and copy public SSH key
  from there
- Choose 'Manage SSH keys' in the 'Clone Repository' dialog and add a
  public SSH key there

After these actions a push to pagure project will automatically be
mirrored to Azure Repos repository.

Now a pipeline can be set up in Azure by picking up azure-pipelines.yml
file from the repo.
